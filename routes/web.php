<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/user/{id?}', function ($id = null) {
   return dd($id);
   //dd($id);
    $arr = [
        1 => "uno",
        2 => "dos"
    ];
    $arr2 = ["nombre"=> "Marcelo","apellido"=>"Pincheira"];
    //dd($arr2);
})->where('id','[0-5]');

Route::get('/route/with/name', function () {
    
})->name('rutaConNombre');

Route::get('/world', function () {
    return "welcome";
})->name('holaMundo');

Route::get('/hello', function () {
    return redirect()->route('holaMundo');
});


Route::group(['prefix' => 'admin'], function () {

   Route::get('/inicio', function () {
    return 'welcome Agilado';
   });

   Route::get('/fin', function () {
    return 'salida Agilado';
   });

});

Route::get('/vista', function () {
    $texto = "texto a mostrar en vista";
    $arr2 = ["nombre"=> "Marcelo","apellido"=>"Pincheira"];
    return view('modules.vista',["textoenvista"=>$arr2]);
});

Route::get('/tasks/{task}', "TaskController@showTask");

